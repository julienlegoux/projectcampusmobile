import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const T_ARTICLES = [
      { ID: 1, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
      { ID: 2, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
      { ID: 3, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
      { ID: 4, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
      { ID: 5, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
      { ID: 6, TITLE: 'Test 1 blabla blabla', IMAGE: 'url', DATE: '01/01/0101', AUTHOR: 'efzfefz',
        CONTENT: 'ves sssssssssssssssssss ssssssssssssssss sssssssssssssssss ssssssssssss sssssssssssss ssssssssssssss ssss' +
        'vesssssss sssssssssssss ssssssssssss sssssssssssss ssssssssss ssssssssss ssssssssss sssssssssssssssss sssssssssssssss sssvessss' +
        'vseeeeee eeeeeeeeee eeeeeeeee eeeeeeeeeeeee  eeeeeeeeee eeeee eeeeeeeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeee' +
        'vseeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeeeeeeeeeeeee sssssssssssssssssse'},
    ];
    const T_USERS = [
      {ID: 1, FIRST_NAME: 'toto', LAST_NAME: 'fzefezf', EMAIL: 'zefzef@zgzeg.fr', COMPAGNY: 'fzezfzef', DIRECT_PHONE: '0101010101',
        CELL_PHONE: '0606060606', JOB_TITLE: 'dfbdfbfddbdfb', BADGE: 'url', INSCRIPTION: 'y', CONTACTED: 'y', DOC_REQUEST: 'y', SEXE: 'f',
      PASSWORD: '123'},
      {ID: 2, FIRST_NAME: 'toto', LAST_NAME: 'fzefezf', EMAIL: 'zefzef@zgzeg.fr', COMPAGNY: 'fzezfzef', DIRECT_PHONE: '0101010101',
        CELL_PHONE: '0606060606', JOB_TITLE: 'dfbdfbfddbdfb', BADGE: 'url', INSCRIPTION: 'y', CONTACTED: 'y', DOC_REQUEST: 'y', SEXE: 'f',
      PASSWORD: '123'},
      {ID: 3, FIRST_NAME: 'toto', LAST_NAME: 'fzefezf', EMAIL: 'zefzef@zgzeg.fr', COMPAGNY: 'fzezfzef', DIRECT_PHONE: '0101010101',
        CELL_PHONE: '0606060606', JOB_TITLE: 'dfbdfbfddbdfb', BADGE: 'url', INSCRIPTION: 'y', CONTACTED: 'y', DOC_REQUEST: 'y', SEXE: 'f',
      PASSWORD: '123'},
      {ID: 4, FIRST_NAME: 'toto', LAST_NAME: 'fzefezf', EMAIL: 'zefzef@zgzeg.fr', COMPAGNY: 'fzezfzef', DIRECT_PHONE: '0101010101',
        CELL_PHONE: '0606060606', JOB_TITLE: 'dfbdfbfddbdfb', BADGE: 'url', INSCRIPTION: 'y', CONTACTED: 'y', DOC_REQUEST: 'y', SEXE: 'f',
      PASSWORD: '123'},
      {ID: 5, FIRST_NAME: 'toto', LAST_NAME: 'fzefezf', EMAIL: 'zefzef@zgzeg.fr', COMPAGNY: 'fzezfzef', DIRECT_PHONE: '0101010101',
        CELL_PHONE: '0606060606', JOB_TITLE: 'dfbdfbfddbdfb', BADGE: 'url', INSCRIPTION: 'y', CONTACTED: 'y', DOC_REQUEST: 'y', SEXE: 'f',
      PASSWORD: '123'}
    ];
    const T_EVENTS = [
      {ID: 1, TITLE: 'zegezf', IMAGE: 'url', BEGIN_DATE: '01/01/0101', END_DATE: '03/01/0101'},
      {ID: 2, TITLE: 'zegezf', IMAGE: 'url', BEGIN_DATE: '01/01/0101', END_DATE: '03/01/0101'},
      {ID: 3, TITLE: 'zegezf', IMAGE: 'url', BEGIN_DATE: '01/01/0101', END_DATE: '03/01/0101'},
      {ID: 4, TITLE: 'zegezf', IMAGE: 'url', BEGIN_DATE: '01/01/0101', END_DATE: '03/01/0101'},
      {ID: 5, TITLE: 'zegezf', IMAGE: 'url', BEGIN_DATE: '01/01/0101', END_DATE: '03/01/0101'}
    ];
    return {T_ARTICLES, T_USERS, T_EVENTS};
  }
}
